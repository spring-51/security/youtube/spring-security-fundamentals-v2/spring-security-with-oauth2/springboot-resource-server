package com.replaceme.server.resource.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableResourceServer
// lesson - 30 - start
@EnableGlobalMethodSecurity(prePostEnabled = true)
// lesson - 30 - start
public class MyResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Value(value = "${my.resourceserver.public.key: invalidKey}")
    private String publicKey;

    /**
     *
     * [NOT A GOOD PRACTICE BUT WIDELY USED]
     * This bean will be used to validate jwt token
     * ONLY AND ONLY IF
     * we have commented or don't use below property in properties file
     * "security.oauth2.resource.token-info-uri"
     *
     * [GOOD PRACTICE]
     * ELSE
     * whatever url we have specified against "security.oauth2.resource.token-info-uri"
     * will be used to validate token
     */
    @Bean
    public TokenStore tokenStore(){
        return new JwtTokenStore(converter());
    }

    @Bean
    public JwtAccessTokenConverter converter(){
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();

        // secret config for SYMMETRIC key - start

        /**
         * IN CASE OF SYMMETRIC KEY
         * THE below signing key MUST BE SAME AS AUTH SERVER(BAD PRACTICE)
         *
         * To overcome above bad practise we should use ASYMMETRIC KEY(will be studied in lesson 18)
         * i.e. auth server will have SECRET KEY, which can be used to CREATE and VALIDATE token
         * and all resource server will have PUBLIC KEY, which CAN VALIDATE token but CANNOT create token
         */
        /**
         * Both the above are BAD PRACTICE,
         * as GOOD PRACTICE if our project architecture permits we should always validate token from
         * auth server
         * i.e.
         * keep auth server check token url against "security.oauth2.resource.token-info-uri"
         * (refer comment on TokenStore bean above or refer application.properties)
         */
        // un-commented for lesson -26 - start
        converter.setSigningKey("123456789012345678901234567890123456");
        // un-commented for lesson -26 - end

        // secret config for SYMMETRIC key - end

        // secret config for ASYMMETRIC key - start
        // this verifier key is public key obtained from jks file using key tool command
        // refer - README.md -> lesson 18
        // commented for lesson -26 - start
        //converter.setVerifierKey(publicKey);
        // commented for lesson -26 - end
        // secret config for ASYMMETRIC key - end


        return converter;
    }

    /**
     * this is OPTIONAL BUT WE KEPT IT KNOW THIS CONFIG METHOD CONFIGS
     * TOKEN STORE
     */
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.tokenStore(tokenStore());
    }

    /**
     * this method has same signature as of WebSecurityConfigurerAdapter -> configure(HttpSecurity http)
     * it's used when we we want to secure end point based on authority or role in
     * spring resource
     * @param http
     * @throws Exception
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .mvcMatchers("/hello/**")
                .hasAnyAuthority("write","read");

        super.configure(http);
    }
}
