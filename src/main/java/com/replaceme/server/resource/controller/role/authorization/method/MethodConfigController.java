package com.replaceme.server.resource.controller.role.authorization.method;

import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/method")
public class MethodConfigController {

    @GetMapping
    /*
    in order to understand more about Pre/ Post annotation refer
    - https://gitlab.com/spring-51/security/youtube/spring-security-fundamentals-v2/spring-secuity-without-oauth2.git
    - You tube -> Spring security fundamentals
     */
    @PostAuthorize(value = "hasAnyAuthority('admin')")
    public String get(Authentication authentication){
        System.out.println("MethodConfigController -> get () - start");
        System.out.println("MethodConfigController -> get () - current user authorities - "+authentication.getAuthorities());
        return "MethodConfigController -> get()";
    }
}
