## lesson 18

```lesson

- in the auth server we have added jwt converter key-value pair
for secret and public key. Here in the resource server we will
use public key.

- public key is generally given by auth server by using jks file.
- in our example we will generate public ke our self using the
myresourceserverkeypair.jks of auth server.
  -- this file is placed in class path of auth-server git repo

- keytool command to create public key using jks file
> keytool -list -rfc --keystore myresourceserverkeypair.jks | openssl x509 -inform pem -pubkey
  -- the above command will give response , copy the public key from it and paste it as verifier key
    --- refer
       - application.properties -> my.resourceserver.public.key
       - MyResourceServerConfig -> converter()

```

## lesson 19
```lesson
- here resource server will call auth server exposed api to get public key, in lesson 19
resource server will not store public-key to validate token , instead
resource server will call auth server to get public key, then using public key
resource server will validate jwt.

- auth server exposed api
  - GET {authServerBareUrl}/oauth/token_key
  eg. http://127.0.0.1:8080/oauth/token_key


Advantages
- when resource server uses auth server exposed api to get public key,
then on rotation of private key of auth server, resource server need not to change anything
as it DOES NOT STORE PUBLIC KEY.

  -- refer
    -- application.properties -> "security.oauth2.resource.jwt.key-uri"
    -- MyResourceServerConfig
```

## lesson 21
```lesson
From here we started second way of implementing resource server i.e
**Spring Security DSL method to create Resource Server** .

from lesson-21 branch the code will very different from previous branches.
it is similar to code from scratch.

Note:
**
THIS RESOURCE SERVER IS WORKING FOR OPAQUE AND ASYMMTRIC KEY, BUT ITS NOT ALIGNED
WITH THE LESSON EXPLAINED IN THE VIDEO, PLEASE RE-REFER.
**
```

## lesson 26
```lesson
- Here we have changed resource server to perform authorization based on endpoint(PATH + HTTP VERB)
[IMPORTANT]
- To do that override configure(HttpSecurity http) of ResourceServerConfigurerAdapter
  **not the configure(HttpSecurity http) of WebSecurityConfigurerAdapter**
  - refer  ResourceServerConfigurerAdapter -> configure(HttpSecurity http)
```


## lesson 30
```lesson
- here we have added Global method security using @PostAuthorize
- refer MethodConfigController -> get()
/*
in order to understand more about Pre/ Post annotation refer
    - https://gitlab.com/spring-51/security/youtube/spring-security-fundamentals-v2/spring-secuity-without-oauth2.git
    - You tube -> Spring security fundamentals

*
```